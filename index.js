const initCanvas = (id) => {
    return new fabric.Canvas(id, {
        width: 1000,
        height: 800,
    });
}

const setBackground = (url, canvas) => {
    fabric.Image.fromURL(url, function(img, isError) {
        img.set({scaleX: canvas.width / img.width, scaleY: canvas.height / img.height,width: canvas.width, height: canvas.height, originX: 'left', originY: 'top'});
        canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
     });
}

const setEdiatbleText = (canvas, plainText, options) => {
    var text = new fabric.IText(plainText, options);
    canvas.add(text);
}

function convertToImagen(id) {
    $("#"+id).get(0).toBlob(function (blob) {
        saveAs(blob, ".png");
    });
}



$( "#birthday_btn" ).on( "click", function() {
    $(".birthday_list").show( "slow" );
    $(".wedding_list").hide();
});

$("#wedding_btn" ).on( "click", function() {
    $(".wedding_list").show( "slow" );
    $(".birthday_list").hide();
});

$("#birthday_btn").click();








